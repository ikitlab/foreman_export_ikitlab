require "erb"
require "foreman/export"

class Foreman::Export::IkitlabRunit < Foreman::Export::Base

  def export_template(name, file=nil, template_root=nil)
    name_without_first = name.split("/")[1..-1].join("/")
    matchers = []
    matchers << File.join(options[:template], name_without_first) if options[:template]
    matchers << File.expand_path("~/.foreman/templates/#{name}")
    matchers << File.expand_path("../../../../data/export/#{name}", __FILE__)
    File.read(matchers.detect { |m| File.exists?(m) })
  end

  def export
    engine.each_process do |name, process|
      1.upto(engine.formation[name]) do |num|
        process_directory = "#{app}-#{name}-#{num}"
        port = engine.port_for(process, num)

        create_directory process_directory
        create_directory "#{process_directory}/env"
        create_directory "#{process_directory}/log"

        write_template "ikitlab_runit/run.erb", "#{process_directory}/run", binding
        chmod 0755, "#{process_directory}/run"

        engine.env.merge("PORT" => port.to_s).each do |key, value|
          write_file "#{process_directory}/env/#{key}", value
        end

        write_template "ikitlab_runit/log/run.erb", "#{process_directory}/log/run", binding
        chmod 0755, "#{process_directory}/log/run"
      end
    end
  end

end
