$:.unshift File.expand_path("../lib", __FILE__)

Gem::Specification.new do |gem|
  gem.name     = "foreman_export_ikitlab"
  gem.version  = '0.0.4'

  gem.author   = "Ilya Krasilnikov"
  gem.email    = "ilya@ikitlab.com"
  gem.homepage = "https://bitbucket.org/ikitlab/foreman_export_ikitlab/"
  gem.summary  = ""

  gem.description = gem.summary

  gem.files = Dir["**/*"].select { |d| d =~ %r{^(README|data/|lib/)} }

  gem.add_dependency 'foreman'
end
